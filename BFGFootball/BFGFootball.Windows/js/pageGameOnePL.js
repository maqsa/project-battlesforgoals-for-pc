﻿
(function () {
    "use strict";
    var local = WinJS.Application.local;
    var app = WinJS.Application;
    var seatsPl1 = [true, true, true, true, true];// player position
    var seatsPl2 = [true, true, true, true, true];// computer position
                       var statusColoda = "close";// deck player
                  var statusColoda_comp = "close";// deck comp
                  var goPl1 = true, goPl2 = false;// first move
    var PL1mustGO = false, PL2mustGO = false,
           PL1mustDEF = false, PL2mustDEF = false;// move pl or cp
                 var arrayPL1 = [], arrayPL2 = [];// cards array
    var atkPL1 = [], atkPL2 = [],
                         defPL1 = [], defPL2 = [];// arr power cards
    var defCard_infoPL1 = [], defCard_infoPL2 = [],
        atkCard_infoPL1 = [], atkCard_infoPL2 = [];// info about cards
             var numb_click, seat_i = -1, goTo = 5;// count
    var owned_card = [], cardBattlePL1 = [],
         cardBattlePL2 = [], owned_cardBattle = [];// array battle
                    var totalPL1 = 0, totalPL2 = 0;// goals
                             var battle_vs = false;// variable determining the dispute
    var tmp_seatPL2 = false, tmp_seatPL1 = false,
   tmp_seatPlayPL = false, tmp_play_compPL = false;// variable for battle
                                          var CARD;// first card array
                           var backDeck, frontDeck;// themes

    app.oncheckpoint = function (args) {
        // TODO: Это приложение будет приостановлено. Сохраните здесь все состояния,
        // которые необходимо сохранять во время приостановки. Можно использовать
        // объект WinJS.Application.sessionState, который автоматически
        // сохраняется и восстанавливается при приостановке. Если перед приостановкой приложения необходимо
        // выполнить асинхронную операцию, вызовите метод
        // args.setPromise().
    };
    ////////////////////////////////////////////РОЗТАНОВКА///////////////////////////


    //функції ПО ПОЗИЦІЯМ
    // ГРАВЦЯ
    function click_Position(a, arr) {

        if (seatsPl1[a] == true) {
            if (statusColoda == "open") {
                document.getElementById("seatPL1" + a).src = frontDeck + arr[0] + ".gif";
                powerPL1Def(arr[0], a);
                document.getElementById("imgCardPl1").src = backDeck;
                statusColoda = "close";
                seatsPl1[a] = false;

                arr.splice(0, 1);
                document.getElementById("count_card1").innerHTML = "Player cards: " + arr.length;
                document.getElementById("count_card1").style.visibility = "visible";
            }
        } else document.getElementById("chat").innerHTML = "Judge: Player not your position";

        if (out_Action() != "positioning") {
            goPl1 = false; goPl2 = true;
            firstGoPL2();
        }
    }
    //КОМПА
    function click_Position_comp(a, arr2) {

        if (seatsPl2[a] == true) {
            document.getElementById("seatPL2" + a).src = frontDeck + arr2[0] + ".gif";
                powerPL2Def(arr2[0], a);

                document.getElementById("imgCardPl2").src = backDeck;
                statusColoda_comp = "close";
                seatsPl2[a] = false;
                arr2.splice(0, 1);
                document.getElementById("count_card2").innerHTML = "Computer cards: " + arr2.length;
        }

        if (out_Action_сomp() != "positioning") {
            goPl1 = false; goPl2 = false; document.getElementById("chat").innerHTML = "Judge: Player attack";
            PL1mustGO = true;
            setTimeout(function () {
                chat.innerHTML = ""; 
            }, 1000); PL1_GO();
        }
    }


    // ДІЇ при натисканні на позиції гравця
    function firstGoPL1() {
        document.getElementById("seatPL10").onclick = function (evt) {
            if (goPl1 == true && statusColoda == "close")
                document.getElementById("chat").innerHTML = "Judge: Player open deck";
            if (goPl1 == true && statusColoda == "open")
            {/*powerPL1Def(arrayPL1[0], 0); */click_Position(0, arrayPL1);
            }
            setTimeout(function () { chat.innerHTML = "" }, 1000);
        }
        document.getElementById("seatPL11").onclick = function (evt) {
            if (goPl1 == true && statusColoda == "close") document.getElementById("chat").innerHTML = "Judge: Player open deck";
            if (goPl1 == true && statusColoda == "open") if (seatsPl1[0] == false)
                click_Position(1, arrayPL1);
            else document.getElementById("chat").innerHTML = "Judge: Player not your position";
            setTimeout(function () { chat.innerHTML = "" }, 1000);
        }
        document.getElementById("seatPL12").onclick = function (evt) {
            if (goPl1 == true && statusColoda == "close") document.getElementById("chat").innerHTML = "Judge: Player open deck";
            if (goPl1 == true && statusColoda == "open") if (seatsPl1[0] == false)
                click_Position(2, arrayPL1);
            else document.getElementById("chat").innerHTML = "Judge: Player not your position";
            setTimeout(function () { chat.innerHTML = "" }, 1000);
        }
        document.getElementById("seatPL13").onclick = function (evt) {
            if (goPl1 == true && statusColoda == "close") document.getElementById("chat").innerHTML = "Judge: Player open deck";
            if (goPl1 == true && statusColoda == "open") if (seatsPl1[0] == false)
                click_Position(3, arrayPL1);
            else document.getElementById("chat").innerHTML = "Judge: Player not your position";
            setTimeout(function () { chat.innerHTML = "" }, 1000);
        }
        document.getElementById("seatPL14").onclick = function (evt) {
            if (goPl1 == true && statusColoda == "close") document.getElementById("chat").innerHTML = "Judge: Player open deck";
            if (goPl1 == true && statusColoda == "open") if (seatsPl1[3] == false && seatsPl1[2] == false && seatsPl1[1] == false)
                click_Position(4, arrayPL1);
            else document.getElementById("chat").innerHTML = "Judge: Player not your position";
            setTimeout(function () { chat.innerHTML = "" }, 1000);

        }
        document.getElementById("imgCardPl1").onclick = function (evt) {
            if (out_Action() == "positioning" && goPl1) {
                statusColoda = "open"; 
                document.getElementById("imgCardPl1").src = frontDeck + arrayPL1[0] + ".gif";
            }
        }
            document.getElementById("seatPL24").addEventListener("click", msg, false);
            document.getElementById("seatPL23").addEventListener("click", msg, false);
            document.getElementById("seatPL22").addEventListener("click", msg, false);
            document.getElementById("seatPL21").addEventListener("click", msg, false);
            document.getElementById("seatPL20").addEventListener("click", msg, false);
            document.getElementById("imgCardPl2").addEventListener("click", msg, false);

    }
    //ЗАКІНЧЕННЯ дій при натисканні на позицію гравця
    function msg() {
        if (goPl1) {
        document.getElementById("chat").innerHTML = "Judge: Player not your position";
        setTimeout(function () { chat.innerHTML = "" }, 1000);
        }
    }
    // ДІЇ при натисканні на позиції компа
    function firstGoPL2() {
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // ДІЇ при натисканні на позиції компа
        for (var ii = 0; ii < seatsPl2.length; ii++)
                click_Position_comp(ii, arrayPL2);
      
 
    }
    //ЗАКІНЧЕННЯ дій при натисканні на позицію компа
    ////////////////////////////////////////////////////////////////////////// ЗАКІНЧЕННЯ ПЕРШОЇ РОЗТАНОВКИ КАРТ. Функції більше не визиваються.

    /////////////////////////НАЧАЛО ГРИ

    function out_Action() {

        if (seatsPl1[0] == true || seatsPl1[1] == true || seatsPl1[2] == true || seatsPl1[3] == true || seatsPl1[4] == true) return "positioning";

    }
    function out_Action_сomp() {

        if (seatsPl2[0] == false && seatsPl2[1] == false && seatsPl2[2] == false && seatsPl2[3] == false && seatsPl2[4] == false) return "game";
        else return "positioning";

    }

    function powerPL1Def(ind, seat) {
        defCard_infoPL1[seat] = ind;
        if (ind == 0 || ind == 9 || ind == 18 || ind == 27)
        { defPL1[seat] = 6; }
        if (ind == 1 || ind == 10 || ind == 19 || ind == 28)
        { defPL1[seat] = 7; }
        if (ind == 2 || ind == 11 || ind == 20 || ind == 29)
        { defPL1[seat] = 8; }
        if (ind == 3 || ind == 12 || ind == 21 || ind == 30)
        { defPL1[seat] = 9; }
        if (ind == 4 || ind == 13 || ind == 22 || ind == 31)
        { defPL1[seat] = 10; }
        if (ind == 5 || ind == 14 || ind == 23 || ind == 32)
        { defPL1[seat] = 11; }
        if (ind == 6 || ind == 15 || ind == 24 || ind == 33)
        { defPL1[seat] = 12; }
        if (ind == 7 || ind == 16 || ind == 25 || ind == 34)
        { defPL1[seat] = 13; }
        if (ind == 8 || ind == 17 || ind == 26 || ind == 35)
        { defPL1[seat] = 14; }
    }
    function powerPL2Def(ind, seat) {
        defCard_infoPL2[seat] = ind;
        if (ind == 0 || ind == 9 || ind == 18 || ind == 27)
        { defPL2[seat] = 6; }
        if (ind == 1 || ind == 10 || ind == 19 || ind == 28)
        { defPL2[seat] = 7; }
        if (ind == 2 || ind == 11 || ind == 20 || ind == 29)
        { defPL2[seat] = 8; }
        if (ind == 3 || ind == 12 || ind == 21 || ind == 30)
        { defPL2[seat] = 9; }
        if (ind == 4 || ind == 13 || ind == 22 || ind == 31)
        { defPL2[seat] = 10; }
        if (ind == 5 || ind == 14 || ind == 23 || ind == 32)
        { defPL2[seat] = 11; }
        if (ind == 6 || ind == 15 || ind == 24 || ind == 33)
        { defPL2[seat] = 12; }
        if (ind == 7 || ind == 16 || ind == 25 || ind == 34)
        { defPL2[seat] = 13; }
        if (ind == 8 || ind == 17 || ind == 26 || ind == 35)
        { defPL2[seat] = 14; }
    }

    function powerPL1Atk(ind, seat) {
        atkCard_infoPL1[seat] = ind;
        if (ind == 0 || ind == 9 || ind == 18 || ind == 27)
        { atkPL1[seat] = 6; }
        if (ind == 1 || ind == 10 || ind == 19 || ind == 28)
        { atkPL1[seat] = 7; }
        if (ind == 2 || ind == 11 || ind == 20 || ind == 29)
        { atkPL1[seat] = 8; }
        if (ind == 3 || ind == 12 || ind == 21 || ind == 30)
        { atkPL1[seat] = 9; }
        if (ind == 4 || ind == 13 || ind == 22 || ind == 31)
        { atkPL1[seat] = 10; }
        if (ind == 5 || ind == 14 || ind == 23 || ind == 32)
        { atkPL1[seat] = 11; }
        if (ind == 6 || ind == 15 || ind == 24 || ind == 33)
        { atkPL1[seat] = 12; }
        if (ind == 7 || ind == 16 || ind == 25 || ind == 34)
        { atkPL1[seat] = 13; }
        if (ind == 8 || ind == 17 || ind == 26 || ind == 35)
        { atkPL1[seat] = 14; }
    }

    function powerPL2Atk(ind, seat) {
        atkCard_infoPL2[seat] = ind;
        if (ind == 0 || ind == 9 || ind == 18 || ind == 27)
        { atkPL2[seat] = 6; }
        if (ind == 1 || ind == 10 || ind == 19 || ind == 28)
        { atkPL2[seat] = 7; }
        if (ind == 2 || ind == 11 || ind == 20 || ind == 29)
        { atkPL2[seat] = 8; }
        if (ind == 3 || ind == 12 || ind == 21 || ind == 30)
        { atkPL2[seat] = 9; }
        if (ind == 4 || ind == 13 || ind == 22 || ind == 31)
        { atkPL2[seat] = 10; }
        if (ind == 5 || ind == 14 || ind == 23 || ind == 32)
        { atkPL2[seat] = 11; }
        if (ind == 6 || ind == 15 || ind == 24 || ind == 33)
        { atkPL2[seat] = 12; }
        if (ind == 7 || ind == 16 || ind == 25 || ind == 34)
        { atkPL2[seat] = 13; }
        if (ind == 8 || ind == 17 || ind == 26 || ind == 35)
        { atkPL2[seat] = 14; }
    }
    function goToPlayAgain() {
        document.location.href = "/html/pageGameOnePL.html";
    }
    function goToHomeMenu() {
        document.location.href = "/default.html";
    }
    function gameOver() {
        PL1mustGO = false, PL2mustGO = false, PL1mustDEF = false, PL2mustDEF = false;
        document.getElementById("chat").innerHTML = "GAME OVER";
        toggleExpandOrCollapse();
        if(totalPL1>totalPL2)
            gameOverWin.innerHTML = "PLAYER WON";
        if(totalPL1<totalPL2)
            gameOverWin.innerHTML = "COMPUTER WON";
        if(totalPL1==totalPL2)
            gameOverWin.innerHTML = "DRAW";

    }
    function showMenu(){            
        gameMenuOnline.addEventListener("click", toggleExpandOrCollapse, false);
        MenuAfterGame.style.display = "none";
        playAgain.addEventListener("click", goToPlayAgain, false);
        homeMenu.addEventListener("click", goToHomeMenu, false);}
    function findDef_comp(a) {

        if (seatsPl2[a] == true) {
            if (statusColoda_comp == "open") {
                document.getElementById("seatPL2" + a).src = frontDeck + arrayPL2[0] + ".gif";
                powerPL2Def(arrayPL2[0], a);
                document.getElementById("imgCardPl2").src = backDeck;
                statusColoda_comp = "close";
                seatsPl2[a] = false;
                arrayPL2.splice(0, 1);
                document.getElementById("count_card2").innerHTML = "Computer cards: " + arrayPL2.length;
            }
        } 
        if (arrayPL2.length == 0)
            gameOver();
        else {
            if (out_Action_сomp() != "positioning") {
                document.getElementById("chat").innerHTML = "Judge: Computer attack";
                PL2mustGO = true; PL1mustGO = false; PL1mustDEF = false; PL2mustDEF = false;
                setTimeout(function () {
                    chat.innerHTML = ""; PL2_GO();
                }, 1000);
            }
        }

    }
    function findDef(a) {

        if (seatsPl1[a] == true) {
            if (statusColoda == "open") {
                document.getElementById("seatPL1" + a).src = frontDeck + arrayPL1[0] + ".gif";
                powerPL1Def(arrayPL1[0], a);
                document.getElementById("imgCardPl1").src = backDeck;
                statusColoda = "close";
                seatsPl1[a] = false;
                arrayPL1.splice(0, 1);
                document.getElementById("count_card1").innerHTML = "Player cards: " + arrayPL1.length;
            }
        }
        if (arrayPL1.length == 0)
            gameOver();
        else {
            if (out_Action() != "positioning") {
                document.getElementById("chat").innerHTML = "Judge: Player attack";
                PL1mustGO = true; PL2mustGO = false; PL1mustDEF = false; PL2mustDEF = false;
                setTimeout(function () {
                    chat.innerHTML = ""; 
                }, 1000); PL1_GO();
            }
        }
    }

    function battle_card(seat_i, seat_idef) {
        tmp_seatPlayPL = false;
        tmp_seatPL2 = false;
        battle_vs = true;
        document.getElementById("playPL" + seat_idef).src = frontDeck + atkCard_infoPL1[seat_i] + ".gif"; // виводимо 1-у карту атаки в спорі
        document.getElementById("seatPL2" + seat_idef).src = frontDeck + defCard_infoPL2[seat_idef] + ".gif";
        owned_cardBattle[owned_cardBattle.length] = atkCard_infoPL1[seat_i];
        owned_cardBattle[owned_cardBattle.length] = defCard_infoPL2[seat_idef];
        seatsPl2[seat_idef] = true;
        if (arrayPL1.length == 0) {
            for (var ii = 0; ii < owned_cardBattle.length; ii++)
                arrayPL2.push(owned_cardBattle[ii]);
            battle_vs = false;
            cardBattlePL1 = []; cardBattlePL2 = []; owned_cardBattle = [];
            afterIfLosePL1(seat_idef);
        } else{
        document.getElementById("imgCardPl1").onclick = function (evt) {
            if (PL1mustGO && statusColoda == "close" && battle_vs) {
                    statusColoda = "open";
                    document.getElementById("imgCardPl1").src = frontDeck + arrayPL1[0] + ".gif";
                    cardBattlePL1[cardBattlePL1.length] = arrayPL1.splice(0, 1);
                    document.getElementById("count_card1").innerHTML = "Player cards: " + arrayPL1.length;
            }
        }
        document.getElementById("playPL" + seat_idef).onclick = function (evt) {
            if (PL1mustGO && statusColoda == "open" && battle_vs && tmp_seatPlayPL == false) {
                document.getElementById("seat_back_pl1" + seat_idef).style.visibility = "visible";
                document.getElementById("seat_back_pl1" + seat_idef).src = backDeck;
                statusColoda = "close";
                tmp_seatPlayPL = true;
                document.getElementById("imgCardPl1").src = backDeck;
                document.getElementById("count_card1").style.visibility = "visible";
                document.getElementById("count_card1").innerHTML = "Player cards: " + arrayPL1.length;
                if (arrayPL1.length == 0) {
                    owned_cardBattle[owned_cardBattle.length] = cardBattlePL1;
                    for (var ii = 0; ii < owned_cardBattle.length; ii++)
                        arrayPL2.push(owned_cardBattle[ii]);
                    battle_vs = false;
                    cardBattlePL1 = []; cardBattlePL2 = []; owned_cardBattle = [];
                    afterIfLosePL1(seat_idef);
                }
            }

        }
        document.getElementById("seat_back_pl1" + seat_idef).onclick = function (evt) {
            if (PL1mustGO && statusColoda == "open" && battle_vs && tmp_seatPlayPL) {
                document.getElementById("seat_open_pl1" + seat_idef).style.visibility = "visible";
                document.getElementById("seat_open_pl1" + seat_idef).src = frontDeck + cardBattlePL1[1] + ".gif";
                statusColoda = "close";
                document.getElementById("imgCardPl1").src = backDeck;
                document.getElementById("count_card1").style.visibility = "visible";
                PL1mustGO = false; PL2mustGO = true;
            }
            setTimeout(function () {
                if (PL2mustGO && battle_vs) {
                    if (arrayPL2.length == 0) { gameOver(); } else {
                        cardBattlePL2[cardBattlePL2.length] = arrayPL2.splice(0, 1);
                    }
                }
                if (PL2mustGO && battle_vs && tmp_seatPL2 == false) {
                        document.getElementById("seat_back_pl2" + seat_idef).style.visibility = "visible";
                        document.getElementById("seat_back_pl2" + seat_idef).src = backDeck;
                        tmp_seatPL2 = true;
                        document.getElementById("count_card2").innerHTML = "Computer cards: " + arrayPL2.length;
                        if (arrayPL2.length == 0) { gameOver(); }
                        else cardBattlePL2[cardBattlePL2.length] = arrayPL2.splice(0, 1);
                }
                if (PL2mustGO && battle_vs && tmp_seatPL2) {
                        document.getElementById("seat_open_pl2" + seat_idef).style.visibility = "visible";
                        document.getElementById("seat_open_pl2" + seat_idef).src = frontDeck + cardBattlePL2[1] + ".gif";
                        document.getElementById("count_card2").innerHTML = "Computer cards: " + arrayPL2.length;

                        powerPL1Atk(cardBattlePL1[1], seat_i);
                        powerPL2Def(cardBattlePL2[1], seat_idef);
                        for (var ii = 0; ii < cardBattlePL1.length; ii++)
                            owned_cardBattle[owned_cardBattle.length] = cardBattlePL1[ii];
                        for (var ii = 0; ii < cardBattlePL2.length; ii++)
                            owned_cardBattle[owned_cardBattle.length] = cardBattlePL2[ii];

                        if (atkPL1[seat_i] > defPL2[seat_idef]) {
                            if (atkPL1[seat_i] == 14 && defPL2[seat_idef] == 6) {
                                for (var ii = 0; ii < owned_cardBattle.length; ii++)
                                    arrayPL2.push(owned_cardBattle[ii]);
                                battle_vs = false;
                                cardBattlePL1 = []; cardBattlePL2 = []; owned_cardBattle = [];
                                afterIfLosePL1(seat_idef);
                            } else {
                                for (var ii = 0; ii < owned_cardBattle.length; ii++)
                                    owned_card[owned_card.length] = owned_cardBattle[ii];
                                PL1mustGO = true; PL2mustGO = false; battle_vs = false;
                                cardBattlePL1 = []; cardBattlePL2 = []; owned_cardBattle = [];
                                if (seat_idef == 0) {
                                    document.getElementById("chat").innerHTML = "Judge: GOAL!!!";
                                    totalPL1++;
                                    document.getElementById("out_total_PL1").innerHTML = " goals:" + totalPL1;
                                    document.getElementById("out_total_PL2").innerHTML = " goals:" + totalPL2;
                                    setTimeout(function () { chat.innerHTML = ""; }, 1000)
                                    afterIfLosePL1(0);
                                } else {
                                    document.getElementById("chat").innerHTML = "Judge: Player attack";
                                    setTimeout(function () {
                                        chat.innerHTML = "";
                                    }, 1000);
                                    if (arrayPL1.length == 0)
                                        afterIfLosePL1(0);
                                    else PL1_GO();
                                }

                            }
                        }
                        if (atkPL1[seat_i] < defPL2[seat_idef]) {
                            if (atkPL1[seat_i] == 6 && defPL2[seat_idef] == 14) {
                                for (var ii = 0; ii < owned_cardBattle.length; ii++)
                                    owned_card[owned_card.length] = owned_cardBattle[ii];
                                PL1mustGO = true; PL2mustGO = false; battle_vs = false;
                                cardBattlePL1 = []; cardBattlePL2 = []; owned_cardBattle = [];
                                if (seat_idef == 0) {
                                    document.getElementById("chat").innerHTML = "Judge: GOAL!!!";
                                    totalPL1++;
                                    document.getElementById("out_total_PL1").innerHTML = " goals:" + totalPL1;
                                    document.getElementById("out_total_PL2").innerHTML = " goals:" + totalPL2;
                                    setTimeout(function () { chat.innerHTML = ""; }, 1000);
                                    afterIfLosePL1(0);
                                } else {
                                    document.getElementById("chat").innerHTML = "Judge: Player attack";
                                    setTimeout(function () {
                                        chat.innerHTML = "";
                                    }, 1000);
                                    if (arrayPL1.length == 0)
                                        afterIfLosePL1(0);
                                    else PL1_GO();
                                }
                            } else {
                                for (var ii = 0; ii < owned_cardBattle.length; ii++)
                                    arrayPL2.push(owned_cardBattle[ii]);
                                battle_vs = false;
                                cardBattlePL1 = []; cardBattlePL2 = []; owned_cardBattle = [];
                                afterIfLosePL1(seat_idef);
                            }
                        }

                        if (atkPL1[seat_i] == defPL2[seat_idef]) {
                            setTimeout(function () {
                                powerPL1Atk(cardBattlePL1[0], seat_i);
                                powerPL2Def(cardBattlePL2[0], seat_idef);
                                document.getElementById("seat_back_pl1" + seat_idef).style.zIndex = "4"; //2
                                document.getElementById("seat_back_pl2" + seat_idef).style.zIndex = "3"; //1
                                document.getElementById("seat_back_pl1" + seat_idef).src = frontDeck + cardBattlePL1[0] + ".gif";
                                document.getElementById("seat_back_pl2" + seat_idef).src = frontDeck + cardBattlePL2[0] + ".gif";

                                if (atkPL1[seat_i] > defPL2[seat_idef]) {
                                    if (atkPL1[seat_i] == 14 && defPL2[seat_idef] == 6) {
                                        for (var ii = 0; ii < owned_cardBattle.length; ii++)
                                            arrayPL2.push(owned_cardBattle[ii]);
                                        battle_vs = false;
                                        cardBattlePL1 = []; cardBattlePL2 = []; owned_cardBattle = [];
                                        afterIfLosePL1(seat_idef);
                                    } else {
                                        for (var ii = 0; ii < owned_cardBattle.length; ii++)
                                            owned_card[owned_card.length] = owned_cardBattle[ii];
                                        PL1mustGO = true; PL2mustGO = false; battle_vs = false;
                                        cardBattlePL1 = []; cardBattlePL2 = []; owned_cardBattle = [];
                                        if (seat_idef == 0) {
                                            document.getElementById("chat").innerHTML = "Judge: GOAL!!!";
                                            totalPL1++;
                                            document.getElementById("out_total_PL1").innerHTML = " goals:" + totalPL1;
                                            document.getElementById("out_total_PL2").innerHTML = " goals:" + totalPL2;
                                            setTimeout(function () { chat.innerHTML = ""; }, 1000)
                                            afterIfLosePL1(0);
                                        } else {
                                            document.getElementById("chat").innerHTML = "Judge: Player attack";
                                            setTimeout(function () {
                                                chat.innerHTML = "";
                                            }, 1000);
                                            if (arrayPL1.length == 0)
                                                afterIfLosePL1(0);
                                            else PL1_GO();
                                        }

                                    }
                                }
                                if (atkPL1[seat_i] <= defPL2[seat_idef]) {
                                    if (atkPL1[seat_i] == 6 && defPL2[seat_idef] == 14) {
                                        for (var ii = 0; ii < owned_cardBattle.length; ii++)
                                            owned_card[owned_card.length] = owned_cardBattle[ii];
                                        PL1mustGO = true; PL2mustGO = false; battle_vs = false;
                                        cardBattlePL1 = []; cardBattlePL2 = []; owned_cardBattle = [];
                                        if (seat_idef == 0) {
                                            document.getElementById("chat").innerHTML = "Judge: GOAL!!!";
                                            totalPL1++;
                                            document.getElementById("out_total_PL1").innerHTML = " goals:" + totalPL1;
                                            document.getElementById("out_total_PL2").innerHTML = " goals:" + totalPL2;
                                            setTimeout(function () { chat.innerHTML = ""; }, 1000)
                                            afterIfLosePL1(0);
                                        } else {
                                            document.getElementById("chat").innerHTML = "Judge: Player attack";
                                            setTimeout(function () {
                                                chat.innerHTML = "";
                                            }, 1000);
                                            if (arrayPL1.length == 0)
                                                afterIfLosePL1(0);
                                            else PL1_GO();
                                        }
                                    } else {
                                        for (var ii = 0; ii < owned_cardBattle.length; ii++)
                                            arrayPL2.push(owned_cardBattle[ii]);
                                        battle_vs = false;
                                        cardBattlePL1 = []; cardBattlePL2 = []; owned_cardBattle = [];
                                        afterIfLosePL1(seat_idef);
                                    }

                                }
                            }, 1000);


                        }
                }
            }, 1000);

        }
        }
    }

    function battle_cardComp(seat_i, seat_idef) {
        tmp_play_compPL = false;
        tmp_seatPL1 = false;
        battle_vs = true;
        owned_cardBattle = [];
        document.getElementById("play_compPL" + seat_idef).src = frontDeck + atkCard_infoPL2[seat_i] + ".gif"; // виводимо 1-у карту атаки в спорі
        document.getElementById("seatPL1" + seat_idef).src = frontDeck + defCard_infoPL1[seat_idef] + ".gif";
        owned_cardBattle[owned_cardBattle.length] = atkCard_infoPL2[seat_i];
        owned_cardBattle[owned_cardBattle.length] = defCard_infoPL1[seat_idef];
        seatsPl1[seat_idef] = true;
            if (arrayPL2.length == 0) {
                for (var ii = 0; ii < owned_cardBattle.length; ii++)
                    arrayPL1.push(owned_cardBattle[ii]);
                battle_vs = false;
                cardBattlePL1 = []; cardBattlePL2 = []; owned_cardBattle = [];
                afterIfLosePL2(seat_idef);
            } else {            
        setTimeout(function () {
            if (PL2mustGO && battle_vs && tmp_play_compPL == false) {
                    cardBattlePL2[cardBattlePL2.length] = arrayPL2.splice(0, 1);
                    document.getElementById("comp_seat_back_pl2" + seat_idef).style.visibility = "visible";
                    document.getElementById("comp_seat_back_pl2" + seat_idef).src = backDeck;
                    tmp_play_compPL = true;
                    document.getElementById("count_card2").innerHTML = "Computer cards: " + arrayPL2.length;

                    if (arrayPL2.length == 0) {
                        owned_cardBattle[owned_cardBattle.length] = cardBattlePL2;
                        for (var ii = 0; ii < owned_cardBattle.length; ii++)
                            arrayPL1.push(owned_cardBattle[ii]);
                        battle_vs = false;
                        cardBattlePL1 = []; cardBattlePL2 = []; owned_cardBattle = [];
                        afterIfLosePL2(seat_idef);
                    } 
            }
            if (PL2mustGO && battle_vs && tmp_play_compPL) {
                    cardBattlePL2[cardBattlePL2.length] = arrayPL2.splice(0, 1);
                    document.getElementById("comp_seat_open_pl2" + seat_idef).style.visibility = "visible";
                    document.getElementById("comp_seat_open_pl2" + seat_idef).src = frontDeck + cardBattlePL2[1] + ".gif";
                    document.getElementById("count_card2").innerHTML = "Computer cards: " + arrayPL2.length;
                    if (arrayPL1.length == 0)
                        gameOver();
                    else {
                        PL2mustGO = false; PL1mustGO = true;
                    }
                    
            }

            document.getElementById("imgCardPl1").onclick = function (evt) {
                if (PL1mustGO && statusColoda == "close" && battle_vs) {
                        statusColoda = "open";
                        document.getElementById("imgCardPl1").src = frontDeck + arrayPL1[0] + ".gif";
                        cardBattlePL1[cardBattlePL1.length] = arrayPL1.splice(0, 1);
                        document.getElementById("count_card1").innerHTML = "Player cards: " + arrayPL1.length;
                }
            }
            document.getElementById("seatPL1" + seat_idef).onclick = function (evt) {
                if (PL1mustGO && statusColoda == "open" && battle_vs && tmp_seatPL1 == false) {
                    document.getElementById("comp_seat_back_pl1" + seat_idef).style.visibility = "visible";
                    document.getElementById("comp_seat_back_pl1" + seat_idef).src = backDeck;
                    statusColoda = "close";
                    tmp_seatPL1 = true;
                    document.getElementById("imgCardPl1").src = backDeck;
                    document.getElementById("count_card1").innerHTML = "Player cards: " + arrayPL1.length;
                    if (arrayPL1.length == 0) { gameOver(); }
                }
            }
            document.getElementById("comp_seat_back_pl1" + seat_idef).onclick = function (evt) {
                if (PL1mustGO && statusColoda == "open" && battle_vs && tmp_seatPL1) {
                    document.getElementById("comp_seat_open_pl1" + seat_idef).style.visibility = "visible";
                    document.getElementById("comp_seat_open_pl1" + seat_idef).src = frontDeck + cardBattlePL1[1] + ".gif";
                    statusColoda = "close";
                    document.getElementById("imgCardPl1").src = backDeck;
                    document.getElementById("count_card1").innerHTML = "Player cards: " + arrayPL1.length;
                    powerPL2Atk(cardBattlePL2[1], seat_i);
                    powerPL1Def(cardBattlePL1[1], seat_idef);
                    for (var ii = 0; ii < cardBattlePL2.length; ii++)
                        owned_cardBattle[owned_cardBattle.length] = cardBattlePL2[ii];
                    for (var ii = 0; ii < cardBattlePL1.length; ii++)
                        owned_cardBattle[owned_cardBattle.length] = cardBattlePL1[ii];
                    if (atkPL2[seat_i] > defPL1[seat_idef]) {
                        if (atkPL2[seat_i] == 14 && defPL1[seat_idef] == 6) {
                            for (var ii = 0; ii < owned_cardBattle.length; ii++)
                                arrayPL1.push(owned_cardBattle[ii]);

                            battle_vs = false;
                            cardBattlePL1 = []; cardBattlePL2 = []; owned_cardBattle = [];
                            afterIfLosePL2(seat_idef);
                        } else {
                            for (var ii = 0; ii < owned_cardBattle.length; ii++)
                                owned_card[owned_card.length] = owned_cardBattle[ii];

                            PL2mustGO = true; PL1mustGO = false; battle_vs = false;
                            cardBattlePL1 = []; cardBattlePL2 = []; owned_cardBattle = [];

                            if (seat_idef == 0) {
                                document.getElementById("chat").innerHTML = "Judge: GOAL!!!";
                                totalPL2++;
                                document.getElementById("out_total_PL1").innerHTML = " goals:" + totalPL1;
                                document.getElementById("out_total_PL2").innerHTML = " goals:" + totalPL2;
                                setTimeout(function () {
                                    chat.innerHTML = "";
                                }, 1000); afterIfLosePL2(0);
                            } else {
                                document.getElementById("chat").innerHTML = "Judge: Computer attack";
                                setTimeout(function () {
                                    chat.innerHTML = "";
                                    if (arrayPL2.length == 0)
                                        afterIfLosePL2(0);
                                    else PL2_GO();
                                }, 1000);
                            }
                        }
                    }
                    if (atkPL2[seat_i] < defPL1[seat_idef]) {
                        if (atkPL2[seat_i] == 6 && defPL1[seat_idef] == 14) {
                            for (var ii = 0; ii < owned_cardBattle.length; ii++)
                                owned_card[owned_card.length] = owned_cardBattle[ii];

                            PL2mustGO = true; PL1mustGO = false; battle_vs = false;
                            cardBattlePL1 = []; cardBattlePL2 = []; owned_cardBattle = [];
                            if (seat_idef == 0) {
                                document.getElementById("chat").innerHTML = "Judge: GOAL!!!";
                                totalPL2++;
                                document.getElementById("out_total_PL1").innerHTML = " goals:" + totalPL1;
                                document.getElementById("out_total_PL2").innerHTML = " goals:" + totalPL2;
                                setTimeout(function () {
                                    chat.innerHTML = "";
                                }, 1000); afterIfLosePL2(0);

                            } else {
                                document.getElementById("chat").innerHTML = "Judge: Computer attack";
                                setTimeout(function () {
                                    chat.innerHTML = "";
                                    if (arrayPL2.length == 0)
                                        afterIfLosePL2(0);
                                    else PL2_GO();
                                }, 1000);

                            }
                        } else {
                            for (var ii = 0; ii < owned_cardBattle.length; ii++)
                                arrayPL1.push(owned_cardBattle[ii]);
                            battle_vs = false;
                            cardBattlePL1 = []; cardBattlePL2 = []; owned_cardBattle = [];
                            afterIfLosePL2(seat_idef);
                        }
                    }
                    if (atkPL2[seat_i] == defPL1[seat_idef]) {
                        setTimeout(function () {
                            powerPL2Atk(cardBattlePL2[0], seat_i);
                            powerPL1Def(cardBattlePL1[0], seat_idef);
                            document.getElementById("comp_seat_back_pl1" + seat_idef).style.zIndex = "3"; //1
                            document.getElementById("comp_seat_back_pl2" + seat_idef).style.zIndex = "4";//2
                            document.getElementById("comp_seat_back_pl1" + seat_idef).src = frontDeck + cardBattlePL1[0] + ".gif";
                            document.getElementById("comp_seat_back_pl2" + seat_idef).src = frontDeck + cardBattlePL2[0] + ".gif";

                            if (atkPL2[seat_i] > defPL1[seat_idef]) {
                                if (atkPL2[seat_i] == 14 && defPL1[seat_idef] == 6) {
                                    for (var ii = 0; ii < owned_cardBattle.length; ii++)
                                        arrayPL1.push(owned_cardBattle[ii]);

                                    battle_vs = false;
                                    cardBattlePL1 = []; cardBattlePL2 = []; owned_cardBattle = [];
                                    afterIfLosePL2(seat_idef);
                                } else {
                                    for (var ii = 0; ii < owned_cardBattle.length; ii++)
                                        owned_card[owned_card.length] = owned_cardBattle[ii];

                                    PL2mustGO = true; PL1mustGO = false; battle_vs = false;
                                    cardBattlePL1 = []; cardBattlePL2 = []; owned_cardBattle = [];

                                    if (seat_idef == 0) {
                                        document.getElementById("chat").innerHTML = "Judge: GOAL!!!";
                                        totalPL2++;
                                        document.getElementById("out_total_PL1").innerHTML = " goals:" + totalPL1;
                                        document.getElementById("out_total_PL2").innerHTML = " goals:" + totalPL2;
                                        setTimeout(function () {
                                            chat.innerHTML = "";
                                        }, 1000); afterIfLosePL2(0);
                                    } else {
                                        document.getElementById("chat").innerHTML = "Judge: Computer attack";
                                        setTimeout(function () {
                                            chat.innerHTML = "";
                                            if (arrayPL2.length == 0)
                                                afterIfLosePL2(0);
                                            else PL2_GO();
                                        }, 1000);

                                    }
                                }
                            }
                            if (atkPL2[seat_i] <= defPL1[seat_idef]) {
                                if (atkPL2[seat_i] == 6 && defPL1[seat_idef] == 14) {
                                    for (var ii = 0; ii < owned_cardBattle.length; ii++)
                                        owned_card[owned_card.length] = owned_cardBattle[ii];

                                    PL2mustGO = true; PL1mustGO = false; battle_vs = false;
                                    cardBattlePL1 = []; cardBattlePL2 = []; owned_cardBattle = [];
                                    if (seat_idef == 0) {
                                        document.getElementById("chat").innerHTML = "Judge: GOAL!!!";
                                        totalPL2++;
                                        document.getElementById("out_total_PL1").innerHTML = " goals:" + totalPL1;
                                        document.getElementById("out_total_PL2").innerHTML = " goals:" + totalPL2;
                                        setTimeout(function () {
                                            chat.innerHTML = "";
                                        }, 1000); afterIfLosePL2(0);

                                    } else {
                                        document.getElementById("chat").innerHTML = "Judge: Computer attack";
                                        setTimeout(function () {
                                            chat.innerHTML = "";
                                            if (arrayPL2.length == 0)
                                                afterIfLosePL2(0);
                                            else PL2_GO();
                                        }, 1000);
                                    }
                                } else {
                                    for (var ii = 0; ii < owned_cardBattle.length; ii++)
                                        arrayPL1.push(owned_cardBattle[ii]);
                                    battle_vs = false;
                                    cardBattlePL1 = []; cardBattlePL2 = []; owned_cardBattle = [];
                                    afterIfLosePL2(seat_idef);
                                }
                            }
                        }, 1000);

                    }

                }
            }
        }, 1000);
            }
    }

    function afterIfLosePL1(seats) {
        if (arrayPL2.length == 0)
            gameOver();
        setTimeout(function () {
            for (var push_i = 0; push_i < owned_card.length; push_i++)
                arrayPL1.push(owned_card[push_i]);
            document.getElementById("count_card1").innerHTML = "Player cards: " + arrayPL1.length;
            document.getElementById("count_card2").innerHTML = "Computer cards: " + arrayPL2.length;
            for (var ch = 0; ch < 5; ch++) {
                document.getElementById("seat_back_pl1" + ch).style.visibility = "hidden";
                document.getElementById("seat_back_pl2" + ch).style.visibility = "hidden";
                document.getElementById("seat_open_pl2" + ch).style.visibility = "hidden";
                document.getElementById("seat_open_pl1" + ch).style.visibility = "hidden";
                document.getElementById("comp_seat_back_pl1" + ch).style.visibility = "hidden";
                document.getElementById("comp_seat_back_pl2" + ch).style.visibility = "hidden";
                document.getElementById("comp_seat_open_pl2" + ch).style.visibility = "hidden";
                document.getElementById("comp_seat_open_pl1" + ch).style.visibility = "hidden";
                document.getElementById("play_compPL" + ch).style.visibility = "visible";
                document.getElementById("comp_seat_back_pl1" + ch).style.zIndex = "1"; //3
                document.getElementById("comp_seat_back_pl2" + ch).style.zIndex = "2";//4
                document.getElementById("seat_back_pl1" + ch).style.zIndex = "2";//4
                document.getElementById("seat_back_pl2" + ch).style.zIndex = "1";//3
            }
            owned_card = [];
            atkCard_infoPL1 = [];
            seat_i = -1;
            atkPL1 = [];
            atkPL2 = [];
            atkCard_infoPL2 = [];
            for (var i = 0; i < 5; i++) {
                document.getElementById("playPL" + i).src = "";
                if (seatsPl1[i])
                    document.getElementById("seatPL1" + i).src = "";
            }
            for (var ii = 0; ii < 5; ii++) {
                document.getElementById("seat_back_pl1" + ii).src = "";
                document.getElementById("seat_open_pl1" + ii).src = "";
                document.getElementById("seat_back_pl2" + ii).src = "";
                document.getElementById("seat_open_pl2" + ii).src = "";
            }
            chat.innerHTML = "";
            if (seatsPl2[4] == false) {
                document.getElementById("chat").innerHTML = "Judge: Computer attack";
                PL1mustDEF = false; PL2mustDEF = false; PL1mustGO = false; PL2mustGO = true;
                PL2_GO();
            } else {
                document.getElementById("chat").innerHTML = "Judge: Move the computer";
                PL1mustDEF = false; PL2mustDEF = true; PL1mustGO = false; PL2mustGO = false;
                PL2_DEF();
            }
            setTimeout(function () { chat.innerHTML = ""; }, 1000)

        }, 1000)

    }
    function afterIfLosePL2(seats) {
        if (arrayPL1.length == 0)
            gameOver();
        setTimeout(function () {
            for (var push_i = 0; push_i < owned_card.length; push_i++)
                arrayPL2.push(owned_card[push_i]);
            document.getElementById("count_card2").innerHTML = "Computer cards: " + arrayPL2.length;
            document.getElementById("count_card1").innerHTML = "Player cards: " + arrayPL1.length;
            for (var ch = 0; ch < 5; ch++) {
                document.getElementById("seat_back_pl1" + ch).style.visibility = "hidden";
                document.getElementById("seat_back_pl2" + ch).style.visibility = "hidden";
                document.getElementById("seat_open_pl2" + ch).style.visibility = "hidden";
                document.getElementById("seat_open_pl1" + ch).style.visibility = "hidden";
                document.getElementById("comp_seat_back_pl1" + ch).style.visibility = "hidden";
                document.getElementById("comp_seat_back_pl2" + ch).style.visibility = "hidden";
                document.getElementById("comp_seat_open_pl2" + ch).style.visibility = "hidden";
                document.getElementById("comp_seat_open_pl1" + ch).style.visibility = "hidden";
                document.getElementById("play_compPL" + ch).style.visibility = "hidden";
                document.getElementById("comp_seat_back_pl1" + ch).style.zIndex = "1"; //3
                document.getElementById("comp_seat_back_pl2" + ch).style.zIndex = "2";//4
                document.getElementById("seat_back_pl1" + ch).style.zIndex = "2";//4
                document.getElementById("seat_back_pl2" + ch).style.zIndex = "1";//3
            }
            owned_card = [];
            atkCard_infoPL1 = [];
            seat_i = -1;
            atkPL1 = [];
            atkPL2 = [];
            goTo = 5;
            atkCard_infoPL2 = [];
            for (var i = 0; i < 5; i++) {
                document.getElementById("play_compPL" + i).src = "";
                if (seatsPl2[i])
                    document.getElementById("seatPL2" + i).src = "";
            }
            for (var ii = 0; ii < 5; ii++) {
                document.getElementById("comp_seat_back_pl2" + ii).src = "";
                document.getElementById("comp_seat_open_pl2" + ii).src = "";
                document.getElementById("comp_seat_back_pl1" + ii).src = "";
                document.getElementById("comp_seat_open_pl1" + ii).src = "";
            }
            chat.innerHTML = "";
            if (seatsPl1[4] == false) {
                document.getElementById("chat").innerHTML = "Judge: Player attack";
                PL1mustDEF = false; PL2mustDEF = false; PL2mustGO = false; PL1mustGO = true;
                PL1_GO();
            } else {
                document.getElementById("chat").innerHTML = "Judge: Move the player";
                PL2mustDEF = false; PL1mustDEF = true; PL1mustGO = false; PL2mustGO = false;
                PL1_DEF();
            }
            setTimeout(function () { chat.innerHTML = ""; }, 1000)

        }, 1000)

    }
    function msgERR() {
        document.getElementById("seatPL24").onclick = function (evt) {
            if (PL1mustDEF == true && statusColoda == "close")
                document.getElementById("chat").innerHTML = "Judge: Player open deck";
            if (PL1mustDEF == true && statusColoda == "open")
                document.getElementById("chat").innerHTML = "Judge: Player not your position";
            setTimeout(function () { chat.innerHTML = ""; }, 1000)
        }
        document.getElementById("seatPL23").onclick = function (evt) {
            if (PL1mustDEF == true && statusColoda == "close")
                document.getElementById("chat").innerHTML = "Judge: Player open deck";
            if (PL1mustDEF == true && statusColoda == "open")
                document.getElementById("chat").innerHTML = "Judge: Player not your position";
            setTimeout(function () { chat.innerHTML = ""; }, 1000)
        }
        document.getElementById("seatPL22").onclick = function (evt) {
            if (PL1mustDEF == true && statusColoda == "close")
                document.getElementById("chat").innerHTML = "Judge: Player open deck";
            if (PL1mustDEF == true && statusColoda == "open")
                document.getElementById("chat").innerHTML = "Judge: Player not your position";
            setTimeout(function () { chat.innerHTML = ""; }, 1000)
        }
        document.getElementById("seatPL21").onclick = function (evt) {
            if (PL1mustDEF == true && statusColoda == "close")
                document.getElementById("chat").innerHTML = "Judge: Player open deck";
            if (PL1mustDEF == true && statusColoda == "open")
                document.getElementById("chat").innerHTML = "Judge: Player not your position";
            setTimeout(function () { chat.innerHTML = ""; }, 1000)
        }
        document.getElementById("seatPL20").onclick = function (evt) {
            if (PL1mustDEF == true && statusColoda == "close")
                document.getElementById("chat").innerHTML = "Judge: Player open deck";
            if (PL1mustDEF == true && statusColoda == "open")
                document.getElementById("chat").innerHTML = "Judge: Player not your position";
            setTimeout(function () { chat.innerHTML = ""; }, 1000)
        }

        document.getElementById("seatPL14").onclick = function (evt) {
            if (PL1mustDEF == true && statusColoda == "close")
                document.getElementById("chat").innerHTML = "Judge: Player open deck";
            if (PL1mustDEF == true && statusColoda == "open")
                document.getElementById("chat").innerHTML = "Judge: Player not your position";
            setTimeout(function () { chat.innerHTML = ""; }, 1000)
        }
        document.getElementById("seatPL13").onclick = function (evt) {
            if (PL1mustDEF == true && statusColoda == "close")
                document.getElementById("chat").innerHTML = "Judge: Player open deck";
            if (PL1mustDEF == true && statusColoda == "open")
                document.getElementById("chat").innerHTML = "Judge: Player not your position";
            setTimeout(function () { chat.innerHTML = ""; }, 1000)
        }
        document.getElementById("seatPL12").onclick = function (evt) {
            if (PL1mustDEF == true && statusColoda == "close")
                document.getElementById("chat").innerHTML = "Judge: Player open deck";
            if (PL1mustDEF == true && statusColoda == "open")
                document.getElementById("chat").innerHTML = "Judge: Player not your position";
            setTimeout(function () { chat.innerHTML = ""; }, 1000)
        }
        document.getElementById("seatPL11").onclick = function (evt) {
            if (PL1mustDEF == true && statusColoda == "close")
                document.getElementById("chat").innerHTML = "Judge: Player open deck";
            if (PL1mustDEF == true && statusColoda == "open")
                document.getElementById("chat").innerHTML = "Judge: Player not your position";
            setTimeout(function () { chat.innerHTML = ""; }, 1000)
        }
        document.getElementById("seatPL10").onclick = function (evt) {
            if (PL1mustDEF == true && statusColoda == "close")
                document.getElementById("chat").innerHTML = "Judge: Player open deck";
            if (PL1mustDEF == true && statusColoda == "open")
                document.getElementById("chat").innerHTML = "Judge: Player not your position";
            setTimeout(function () { chat.innerHTML = ""; }, 1000)
        }
    }
    function PL1_GO() {

        /*Вірні натисканні події*/
        document.getElementById("imgCardPl1").onclick = function (evt) {
            if (PL1mustGO && statusColoda == "close") {
                statusColoda = "open";
                CARD = arrayPL1.splice(0, 1);
                document.getElementById("imgCardPl1").src = frontDeck + CARD + ".gif";

            }
        }
        document.getElementById("seatPL24").onclick = function (evt) {

            if (PL1mustGO && statusColoda == "open" && seatsPl2[4] == false && battle_vs == false) {
                seat_i++;
                powerPL1Atk(CARD, seat_i);
                document.getElementById("imgCardPl1").src = backDeck;
                statusColoda = "close";
                document.getElementById("count_card1").style.visibility = "visible";
                document.getElementById("count_card1").innerHTML = "Player cards: " + arrayPL1.length;
                document.getElementById("playPL4").src = frontDeck + CARD + ".gif";
                if (atkPL1[seat_i] > defPL2[4]) {
                    if (atkPL1[seat_i] == 14 && defPL2[4] == 6) {
                        owned_card[owned_card.length] = atkCard_infoPL1[seat_i];
                        PL1mustGO = false;
                        afterIfLosePL1(0);
                    } else {
                        owned_card[owned_card.length] = atkCard_infoPL1[seat_i];
                        owned_card[owned_card.length] = defCard_infoPL2[4];
                        seatsPl2[4] = true;
                        if (arrayPL1.length == 0) {
                            PL1mustGO = false;
                            afterIfLosePL1(0);
                        }
                    }
                }
                if (atkPL1[seat_i] == defPL2[4]) {
                    battle_card(seat_i, 4);//перевірка нових карт на силу
                }
                if (atkPL1[seat_i] < defPL2[4]) {
                    if (atkPL1[seat_i] == 6 && defPL2[4] == 14) {
                        owned_card[owned_card.length] = atkCard_infoPL1[seat_i];
                        owned_card[owned_card.length] = defCard_infoPL2[4];
                        seatsPl2[4] = true;
                        if (arrayPL1.length == 0) {
                            PL1mustGO = false;
                            afterIfLosePL1(0);
                        }
                    } else {
                        owned_card[owned_card.length] = atkCard_infoPL1[seat_i];
                        PL1mustGO = false;
                        afterIfLosePL1(0);
                    }
                }
            } else {
                if (PL1mustGO) {
                    document.getElementById("chat").innerHTML = "Judge: Player wrong move";
                    setTimeout(function () { chat.innerHTML = "" }, 1000);
                }
            }

        }
        document.getElementById("seatPL23").onclick = function (evt) {

            if (PL1mustGO && statusColoda == "open" && seatsPl2[3] == false && seatsPl2[4] && battle_vs == false) {
                seat_i++;
                powerPL1Atk(CARD, seat_i);
                document.getElementById("imgCardPl1").src = backDeck;
                statusColoda = "close";
                document.getElementById("count_card1").style.visibility = "visible";
                document.getElementById("count_card1").innerHTML = "Player cards: " + arrayPL1.length;
                document.getElementById("playPL3").src = frontDeck + CARD + ".gif";

                if (atkPL1[seat_i] > defPL2[3]) {
                    if (atkPL1[seat_i] == 14 && defPL2[3] == 6) {
                        owned_card[owned_card.length] = atkCard_infoPL1[seat_i];
                        PL1mustGO = false;
                        afterIfLosePL1(0);
                    } else {
                        owned_card[owned_card.length] = atkCard_infoPL1[seat_i];
                        owned_card[owned_card.length] = defCard_infoPL2[3];
                        seatsPl2[3] = true;
                        if (arrayPL1.length == 0) {
                            PL1mustGO = false;
                            afterIfLosePL1(0);
                        }
                    }
                }
                if (atkPL1[seat_i] == defPL2[3]) {
                    battle_card(seat_i, 3);//перевірка нових карт на силу
                }
                if (atkPL1[seat_i] < defPL2[3]) {
                    if (atkPL1[seat_i] == 6 && defPL2[3] == 14) {
                        owned_card[owned_card.length] = atkCard_infoPL1[seat_i];
                        owned_card[owned_card.length] = defCard_infoPL2[3];
                        seatsPl2[3] = true;
                        if (arrayPL1.length == 0) {
                            PL1mustGO = false;
                            afterIfLosePL1(0);
                        }
                    } else {
                        owned_card[owned_card.length] = atkCard_infoPL1[seat_i];
                        PL1mustGO = false;
                        afterIfLosePL1(0);
                    }
                }
            } else {
                if (PL1mustGO) {
                    document.getElementById("chat").innerHTML = "Judge: Player wrong move";
                    setTimeout(function () { chat.innerHTML = "" }, 1000);
                }
            }
        }
        document.getElementById("seatPL22").onclick = function (evt) {

            if (PL1mustGO && statusColoda == "open" && seatsPl2[2] == false && seatsPl2[4] && battle_vs == false) {
                seat_i++;
                powerPL1Atk(CARD, seat_i);
                document.getElementById("imgCardPl1").src = backDeck;
                statusColoda = "close";
                document.getElementById("count_card1").style.visibility = "visible";
                document.getElementById("count_card1").innerHTML = "Player cards: " + arrayPL1.length;
                document.getElementById("playPL2").src = frontDeck + CARD + ".gif";

                if (atkPL1[seat_i] > defPL2[2]) {
                    if (atkPL1[seat_i] == 14 && defPL2[2] == 6) {
                        owned_card[owned_card.length] = atkCard_infoPL1[seat_i];
                        PL1mustGO = false;
                        afterIfLosePL1(0);
                    } else {
                        owned_card[owned_card.length] = atkCard_infoPL1[seat_i];
                        owned_card[owned_card.length] = defCard_infoPL2[2];
                        seatsPl2[2] = true;
                        if (arrayPL1.length == 0) {
                            PL1mustGO = false;
                            afterIfLosePL1(0);
                        }
                    }
                }
                if (atkPL1[seat_i] == defPL2[2]) {
                    battle_card(seat_i, 2);//перевірка нових карт на силу
                }
                if (atkPL1[seat_i] < defPL2[2]) {
                    if (atkPL1[seat_i] == 6 && defPL2[2] == 14) {
                        owned_card[owned_card.length] = atkCard_infoPL1[seat_i];
                        owned_card[owned_card.length] = defCard_infoPL2[2];
                        seatsPl2[2] = true;
                        if (arrayPL1.length == 0) {
                            PL1mustGO = false;
                            afterIfLosePL1(0);
                        }
                    } else {
                        owned_card[owned_card.length] = atkCard_infoPL1[seat_i];
                        PL1mustGO = false;
                        afterIfLosePL1(0);
                    }
                }
            } else {
                if (PL1mustGO) {
                    document.getElementById("chat").innerHTML = "Judge: Player wrong move";
                    setTimeout(function () { chat.innerHTML = "" }, 1000);
                }
            }
        }
        document.getElementById("seatPL21").onclick = function (evt) {

            if (PL1mustGO && statusColoda == "open" && seatsPl2[1] == false && seatsPl2[4] && battle_vs == false) {
                seat_i++;
                powerPL1Atk(CARD, seat_i);
                document.getElementById("imgCardPl1").src = backDeck;
                statusColoda = "close";
                document.getElementById("count_card1").style.visibility = "visible";
                document.getElementById("count_card1").innerHTML = "Player cards: " + arrayPL1.length;
                document.getElementById("playPL1").src = frontDeck + CARD + ".gif";
                if (atkPL1[seat_i] > defPL2[1]) {
                    if (atkPL1[seat_i] == 14 && defPL2[1] == 6) {
                        owned_card[owned_card.length] = atkCard_infoPL1[seat_i];
                        PL1mustGO = false;
                        afterIfLosePL1(0);
                    } else {
                        owned_card[owned_card.length] = atkCard_infoPL1[seat_i];
                        owned_card[owned_card.length] = defCard_infoPL2[1];
                        seatsPl2[1] = true;
                        if (arrayPL1.length == 0) {
                            PL1mustGO = false;
                            afterIfLosePL1(0);
                        }
                    }
                }
                if (atkPL1[seat_i] == defPL2[1]) {
                    battle_card(seat_i, 1);//перевірка нових карт на силу
                }
                if (atkPL1[seat_i] < defPL2[1]) {
                    if (atkPL1[seat_i] == 6 && defPL2[1] == 14) {
                        owned_card[owned_card.length] = atkCard_infoPL1[seat_i];
                        owned_card[owned_card.length] = defCard_infoPL2[1];
                        seatsPl2[1] = true;
                        if (arrayPL1.length == 0) {
                            PL1mustGO = false;
                            afterIfLosePL1(0);
                        }
                    } else {
                        owned_card[owned_card.length] = atkCard_infoPL1[seat_i];
                        PL1mustGO = false;
                        afterIfLosePL1(0);
                    }
                }
            } else {
                if (PL1mustGO) {
                    document.getElementById("chat").innerHTML = "Judge: Player wrong move";
                    setTimeout(function () { chat.innerHTML = "" }, 1000);
                }
            }
        }
        document.getElementById("seatPL20").onclick = function (evt) {

            if (PL1mustGO && statusColoda == "open" && seatsPl2[0] == false && (seatsPl2[3] && seatsPl2[2] && seatsPl2[1]) && battle_vs == false) {
                seat_i++;
                powerPL1Atk(CARD, seat_i);
                document.getElementById("imgCardPl1").src = backDeck;
                statusColoda = "close";
                document.getElementById("playPL0").src = frontDeck + CARD + ".gif";
                document.getElementById("count_card1").innerHTML = "Player cards: " + arrayPL1.length;
                if (atkPL1[seat_i] > defPL2[0]) {
                    if (atkPL1[seat_i] == 14 && defPL2[0] == 6) {
                        owned_card[owned_card.length] = atkCard_infoPL1[seat_i];
                        PL1mustGO = false;
                        afterIfLosePL1(0);
                    } else {
                        owned_card[owned_card.length] = atkCard_infoPL1[seat_i];
                        owned_card[owned_card.length] = defCard_infoPL2[0];
                        seatsPl2[0] = true;
                        document.getElementById("chat").innerHTML = "Judge: GOAL!!!";
                        totalPL1++;
                        document.getElementById("out_total_PL1").innerHTML = " goals:" + totalPL1;
                        document.getElementById("out_total_PL2").innerHTML = " goals:" + totalPL2;
                        PL1mustGO = false;
                        setTimeout(function () {
                            chat.innerHTML = "";
                        }, 1000); afterIfLosePL1(0);
                    }
                }
                if (atkPL1[seat_i] == defPL2[0]) {
                    battle_card(seat_i, 0);//перевірка нових карт на силу
                }
                if (atkPL1[seat_i] < defPL2[0]) {
                    if (atkPL1[seat_i] == 6 && defPL2[0] == 14) {
                        owned_card[owned_card.length] = atkCard_infoPL1[seat_i];
                        owned_card[owned_card.length] = defCard_infoPL2[0];
                        seatsPl2[0] = true;
                        document.getElementById("chat").innerHTML = "Judge: GOAL!!!";
                        totalPL1++;
                        document.getElementById("out_total_PL1").innerHTML = " goals:" + totalPL1;
                        document.getElementById("out_total_PL2").innerHTML = " goals:" + totalPL2;
                        PL1mustGO = false;
                        setTimeout(function () {
                            chat.innerHTML = "";
                        }, 1000); afterIfLosePL1(0);
                    } else {
                        owned_card[owned_card.length] = atkCard_infoPL1[seat_i];
                        PL1mustGO = false;
                        setTimeout(function () {
                            chat.innerHTML = "";
                        }, 1000); afterIfLosePL1(0);
                    }
                }
            } else {
                if (PL1mustGO) {
                    document.getElementById("chat").innerHTML = "Judge: Player wrong move";
                    setTimeout(function () { chat.innerHTML = "" }, 1000);
                }
            }
        }
        //Повідомлення про не вірні натискання
        document.getElementById("imgCardPl2").onclick = function (evt) {
            if (PL1mustGO) {
                document.getElementById("chat").innerHTML = "Judge: Player not your position to attack";
                setTimeout(function () { chat.innerHTML = "" }, 1000);
            }
        }
        document.getElementById("seatPL10").onclick = function (evt) {
            if (PL1mustGO) {
                document.getElementById("chat").innerHTML = "Judge: Player not your position to attack";
                setTimeout(function () { chat.innerHTML = "" }, 1000);
            }
        }
        document.getElementById("seatPL11").onclick = function (evt) {
            if (PL1mustGO) {
                document.getElementById("chat").innerHTML = "Judge: Player not your position to attack";
                setTimeout(function () { chat.innerHTML = "" }, 1000);
            }
        }
        document.getElementById("seatPL12").onclick = function (evt) {
            if (PL1mustGO) {
                document.getElementById("chat").innerHTML = "Judge: Player not your position to attack";
                setTimeout(function () { chat.innerHTML = "" }, 1000);
            }
        }
        document.getElementById("seatPL13").onclick = function (evt) {
            if (PL1mustGO) {
                document.getElementById("chat").innerHTML = "Judge: Player not your position to attack";
                setTimeout(function () { chat.innerHTML = "" }, 1000);
            }
        }
        document.getElementById("seatPL14").onclick = function (evt) {
            if (PL1mustGO) {
                document.getElementById("chat").innerHTML = "Judge: Player not your position to attack";
                setTimeout(function () { chat.innerHTML = "" }, 1000);
            }
        }
    }
    function PL2_GO() {
      
        goTo--; seat_i++;
        if (PL2mustGO) {
            //////////////////////////////////////////////////////4
            if (seatsPl1[4] == false) {
                setTimeout(function () {
                    CARD = arrayPL2.splice(0, 1);
                    document.getElementById("count_card2").innerHTML = "Computer cards: " + arrayPL2.length;
                    powerPL2Atk(CARD, seat_i);
                    document.getElementById("play_compPL" + 4).src = frontDeck + CARD + ".gif";
                    if (atkPL2[seat_i] == defPL1[4]) {
                        battle_cardComp(seat_i, 4);//перевірка нових карт на силу
                    }
                    if (atkPL2[seat_i] < defPL1[4]) {
                        if (atkPL2[seat_i] == 6 && defPL1[4] == 14) {
                            owned_card[owned_card.length] = atkCard_infoPL2[seat_i];
                            owned_card[owned_card.length] = defCard_infoPL1[4];
                            seatsPl1[4] = true;
                            if (arrayPL2.length == 0)
                                afterIfLosePL2(0);
                            else PL2_GO();

                        } else {
                            owned_card[owned_card.length] = atkCard_infoPL2[seat_i];
                            afterIfLosePL2(4);
                        }
                    }
                    if (atkPL2[seat_i] > defPL1[4]) {
                        if (atkPL2[seat_i] == 14 && defPL1[4] == 6) {
                            owned_card[owned_card.length] = atkCard_infoPL2[seat_i];
                            afterIfLosePL2(4);
                        } else {
                            owned_card[owned_card.length] = atkCard_infoPL2[seat_i];
                            owned_card[owned_card.length] = defCard_infoPL1[4];
                            seatsPl1[4] = true;
                            if (arrayPL2.length == 0)
                                afterIfLosePL2(0);
                            else PL2_GO();
                        }
                    }
                }, 1000)
            }
            ////////////////////////////////////////////////////////////////////////////////3 or 2 or 1
            if (seatsPl1[4] && (seatsPl1[3] == false || seatsPl1[2] == false || seatsPl1[1] == false)) {

                setTimeout(function () {
                    CARD = arrayPL2.splice(0, 1);
                    document.getElementById("count_card2").innerHTML = "Computer cards: " + arrayPL2.length;
                    powerPL2Atk(CARD, seat_i);
                    if (seatsPl1[3] == false) {////////////////////////////////////////////////////////3
                        if (atkPL2[seat_i] > defPL1[3]) {
                            if (atkPL2[seat_i] == 14 && defPL1[3] == 6) {
                                owned_card[owned_card.length] = atkCard_infoPL2[seat_i];
                                document.getElementById("play_compPL" + 3).src = frontDeck + CARD + ".gif";
                                afterIfLosePL2(3);
                            } else {
                                PL2_GO_winSeats(3);
                            }
                        }
                        if (atkPL2[seat_i] == defPL1[3]) {
                            battle_cardComp(seat_i, 3);//перевірка нових карт на силу
                        }
                        if (atkPL2[seat_i] < defPL1[3]) {
                            if (atkPL2[seat_i] == 6 && defPL1[3] == 14) {
                                PL2_GO_winSeats(3);
                            } else {
                                if (seatsPl1[2] == false) {//////////////////////////2
                                    if (atkPL2[seat_i] > defPL1[2]) {
                                        if (atkPL2[seat_i] == 14 && defPL1[2] == 6) {
                                            owned_card[owned_card.length] = atkCard_infoPL2[seat_i];
                                            document.getElementById("play_compPL" + 2).src = frontDeck + CARD + ".gif";
                                            afterIfLosePL2(4);
                                        } else {
                                            PL2_GO_winSeats(2);
                                        }
                                    }
                                    if (atkPL2[seat_i] == defPL1[2]) {
                                        battle_cardComp(seat_i, 2);//перевірка нових карт на силу
                                    }
                                    if (atkPL2[seat_i] < defPL1[2]) {
                                        if (atkPL2[seat_i] == 6 && defPL1[2] == 14) {
                                            PL2_GO_winSeats(2);
                                        } else {
                                            if (seatsPl1[1] == false) {
                                                if (atkPL2[seat_i] > defPL1[1]) {
                                                    if (atkPL2[seat_i] == 14 && defPL1[1] == 6) {
                                                        owned_card[owned_card.length] = atkCard_infoPL2[seat_i];
                                                        document.getElementById("play_compPL" + 1).src = frontDeck + CARD + ".gif";
                                                        afterIfLosePL2(4);
                                                    } else {
                                                        PL2_GO_winSeats(1);
                                                    }
                                                }
                                                if (atkPL2[seat_i] == defPL1[1]) {
                                                    battle_cardComp(seat_i, 1);//перевірка нових карт на силу
                                                }
                                                if (atkPL2[seat_i] < defPL1[1]) {
                                                    if (atkPL2[seat_i] == 6 && defPL1[1] == 14) {
                                                        PL2_GO_winSeats(1);
                                                    } else {
                                                        owned_card[owned_card.length] = atkCard_infoPL2[seat_i];
                                                        document.getElementById("play_compPL" + 1).src = frontDeck + CARD + ".gif";
                                                        afterIfLosePL2(4);
                                                    }
                                                }
                                            }
                                            else {
                                                owned_card[owned_card.length] = atkCard_infoPL2[seat_i];
                                                document.getElementById("play_compPL" + 2).src = frontDeck + CARD + ".gif";
                                                afterIfLosePL2(4);
                                            }
                                        }
                                    }
                                }
                                else if (seatsPl1[1] == false) {
                                    if (atkPL2[seat_i] > defPL1[1]) {
                                        if (atkPL2[seat_i] == 14 && defPL1[1] == 6) {
                                            owned_card[owned_card.length] = atkCard_infoPL2[seat_i];
                                            document.getElementById("play_compPL" + 1).src = frontDeck + CARD + ".gif";
                                            afterIfLosePL2(4);
                                        } else {
                                            PL2_GO_winSeats(1);
                                        }
                                    }
                                    if (atkPL2[seat_i] == defPL1[1]) {
                                        battle_cardComp(seat_i, 1);//перевірка нових карт на силу
                                    }
                                    if (atkPL2[seat_i] < defPL1[1]) {
                                        if (atkPL2[seat_i] == 6 && defPL1[1] == 14) {
                                            PL2_GO_winSeats(1);
                                        } else {
                                            owned_card[owned_card.length] = atkCard_infoPL2[seat_i];
                                            document.getElementById("play_compPL" + 1).src = frontDeck + CARD + ".gif";
                                            afterIfLosePL2(4);
                                        }
                                    }
                                }
                                else {
                                    owned_card[owned_card.length] = atkCard_infoPL2[seat_i];
                                    document.getElementById("play_compPL" + 3).src = frontDeck + CARD + ".gif";
                                    afterIfLosePL2(4);
                                }

                            }
                        }
                    }
                    else if (seatsPl1[2] == false) {////////////////////////////else 2
                        if (atkPL2[seat_i] > defPL1[2]) {
                            if (atkPL2[seat_i] == 14 && defPL1[2] == 6) {
                                owned_card[owned_card.length] = atkCard_infoPL2[seat_i];
                                document.getElementById("play_compPL" + 2).src = frontDeck + CARD + ".gif";
                                afterIfLosePL2(4);
                            } else {
                                PL2_GO_winSeats(2);
                            }
                        }
                        if (atkPL2[seat_i] == defPL1[2]) {
                            battle_cardComp(seat_i, 2);//перевірка нових карт на силу
                        }
                        if (atkPL2[seat_i] < defPL1[2]) {
                            if (atkPL2[seat_i] == 6 && defPL1[2] == 14) {
                                PL2_GO_winSeats(2);
                            } else {
                                if (seatsPl1[1] == false) {//////////////////////////////1
                                    if (atkPL2[seat_i] > defPL1[1]) {
                                        if (atkPL2[seat_i] == 14 && defPL1[1] == 6) {
                                            owned_card[owned_card.length] = atkCard_infoPL2[seat_i];
                                            document.getElementById("play_compPL" + 1).src = frontDeck + CARD + ".gif";
                                            afterIfLosePL2(4);
                                        } else {
                                            PL2_GO_winSeats(1);
                                        }
                                    }
                                    if (atkPL2[seat_i] == defPL1[1]) {
                                        battle_cardComp(seat_i, 1);//перевірка нових карт на силу
                                    }
                                    if (atkPL2[seat_i] < defPL1[1]) {
                                        if (atkPL2[seat_i] == 6 && defPL1[1] == 14) {
                                            PL2_GO_winSeats(1);
                                        } else {
                                            owned_card[owned_card.length] = atkCard_infoPL2[seat_i];
                                            document.getElementById("play_compPL" + 1).src = frontDeck + CARD + ".gif";
                                            afterIfLosePL2(4);
                                        }
                                    }
                                }
                                else {
                                    owned_card[owned_card.length] = atkCard_infoPL2[seat_i];
                                    document.getElementById("play_compPL" + 2).src = frontDeck + CARD + ".gif";
                                    afterIfLosePL2(4);
                                }
                            }
                        }
                    }
                    else if (seatsPl1[1] == false) {////////////////////////////else 1
                        if (atkPL2[seat_i] > defPL1[1]) {
                            if (atkPL2[seat_i] == 14 && defPL1[1] == 6) {
                                owned_card[owned_card.length] = atkCard_infoPL2[seat_i];
                                document.getElementById("play_compPL" + 1).src = frontDeck + CARD + ".gif";
                                afterIfLosePL2(4);
                            } else {
                                PL2_GO_winSeats(1);
                            }
                        }
                        if (atkPL2[seat_i] == defPL1[1]) {
                            battle_cardComp(seat_i, 1);//перевірка нових карт на силу
                        }
                        if (atkPL2[seat_i] < defPL1[1]) {
                            if (atkPL2[seat_i] == 6 && defPL1[1] == 14) {
                                PL2_GO_winSeats(1);
                            } else {
                                owned_card[owned_card.length] = atkCard_infoPL2[seat_i];
                                document.getElementById("play_compPL" + 1).src = frontDeck + CARD + ".gif";
                                afterIfLosePL2(4);
                            }
                        }
                    }
                }, 1000);
            }
            //////////////////////////////////////////////0
            if (seatsPl1[0] == false && seatsPl1[1] && seatsPl1[1] && seatsPl1[2] && seatsPl1[3]) {
                setTimeout(function () {
                    CARD = arrayPL2.splice(0, 1);
                    powerPL2Atk(CARD, 4);
                    document.getElementById("count_card2").innerHTML = "Computer cards: " + arrayPL2.length;
                    document.getElementById("play_compPL" + 0).src = frontDeck + CARD + ".gif";
                    if (atkPL2[4] > defPL1[0]) {
                        if (atkPL2[4] == 14 && defPL1[0] == 6) {
                            owned_card[owned_card.length] = atkCard_infoPL2[4];
                            afterIfLosePL2(0);
                        } else {
                            owned_card[owned_card.length] = atkCard_infoPL2[4];
                            document.getElementById("play_compPL0").src = frontDeck + CARD + ".gif";
                            owned_card[owned_card.length] = defCard_infoPL1[0];
                            seatsPl1[0] = true;
                            document.getElementById("chat").innerHTML = "Judge: GOAL!!!";
                            totalPL2++;
                            document.getElementById("out_total_PL1").innerHTML = " goals:" + totalPL1;
                            document.getElementById("out_total_PL2").innerHTML = " goals:" + totalPL2;
                            setTimeout(function () { chat.innerHTML = ""; }, 1000);
                            afterIfLosePL2(0);
                        }
                    }
                    if (atkPL2[4] == defPL1[0]) {
                        battle_cardComp(4, 0);//перевірка нових карт на силу
                    }
                    if (atkPL2[4] < defPL1[0]) {
                        if (atkPL2[4] == 6 && defPL1[0] == 14) {
                            owned_card[owned_card.length] = atkCard_infoPL2[4];
                            document.getElementById("play_compPL0").src = frontDeck + CARD + ".gif";
                            owned_card[owned_card.length] = defCard_infoPL1[0];
                            seatsPl1[0] = true;
                            document.getElementById("chat").innerHTML = "Judge: GOAL!!!";
                            totalPL2++;
                            document.getElementById("out_total_PL1").innerHTML = " goals:" + totalPL1;
                            document.getElementById("out_total_PL2").innerHTML = " goals:" + totalPL2;
                            setTimeout(function () { chat.innerHTML = ""; }, 1000); afterIfLosePL2(0);
                        } else {
                            owned_card[owned_card.length] = atkCard_infoPL2[seat_i];
                            setTimeout(function () { chat.innerHTML = ""; }, 1000); afterIfLosePL2(0);
                        }
                    }
                }, 1000);

            }
        }
    }
    function PL2_GO_winSeats(seats) {
        owned_card[owned_card.length] = atkCard_infoPL2[seat_i];
        document.getElementById("play_compPL" + seats).src = frontDeck + CARD + ".gif";
        owned_card[owned_card.length] = defCard_infoPL1[seats];
        seatsPl1[seats] = true;
        if (arrayPL2.length == 0)
            afterIfLosePL2(0);
        else PL2_GO();
    }

    function PL2_DEF() {
        for (var i = 0; i < 5; i++) {
            document.getElementById("playPL" + i).src = "";
            if (seatsPl2[i])
                document.getElementById("seatPL2" + i).src = "";
        }
        if (arrayPL2.length == 0) {
            PL2mustDEF = false;
            gameOver();
        }
        else {
            for (var ii = 0; ii < seatsPl2.length; ii++) {
                if (seatsPl2[ii]) {
                    if (out_Action_сomp() == "positioning" && PL2mustDEF) {
                        statusColoda_comp = "open";
                        if (arrayPL2.length == 0) {
                            PL2mustDEF = false;
                            gameOver();
                        }
                    } findDef_comp(ii);
                }
            }
        }
    }
    function PL1_DEF() {
        for (var i = 0; i < 5; i++) {
            document.getElementById("play_compPL" + i).src = "";
            if (seatsPl1[i])
                document.getElementById("seatPL1" + i).src = "";
        }
        if (arrayPL1.length == 0) {
            PL1mustDEF = false;
            gameOver();
        }
        if (out_Action() == "positioning" && PL1mustDEF) {
            document.getElementById("imgCardPl2").onclick = function (evt) {
                if (PL1mustDEF == true && statusColoda == "close")
                    document.getElementById("chat").innerHTML = "Judge: Player open deck";
                if (PL1mustDEF == true && statusColoda == "open")
                    document.getElementById("chat").innerHTML = "Judge: Player not your position";
                setTimeout(function () { chat.innerHTML = ""; }, 1000)
            }
            msgERR();
            document.getElementById("imgCardPl1").onclick = function (evt) {
                if (statusColoda == "open" && PL1mustDEF) {
                    document.getElementById("chat").innerHTML = "Judge: Player not your position";
                    setTimeout(function () { chat.innerHTML = ""; }, 1000)
                }
                statusColoda = "open";
                document.getElementById("imgCardPl1").src = frontDeck + arrayPL1[0] + ".gif";
                //findDef_comp();	
                if (arrayPL1.length == 0) {
                    PL1mustDEF = false;
                    gameOver();
                } else {
                    document.getElementById("seatPL10").onclick = function (evt) {
                        if (PL1mustDEF == true && statusColoda == "close")
                            document.getElementById("chat").innerHTML = "Judge: Player open deck";
                            setTimeout(function () { chat.innerHTML = "" }, 1000);
                            if (PL1mustDEF == true && statusColoda == "open")
                            {/*powerPL1Def(arrayPL1[0], 0); */findDef(0); } else chat.innerHTML = "Judge: Player not your position";
                    }
                    document.getElementById("seatPL11").onclick = function (evt) {
                        if (PL1mustDEF == true && statusColoda == "close")
                            document.getElementById("chat").innerHTML = "Judge: Player open deck";
                        if (PL1mustDEF == true && statusColoda == "open") if (seatsPl1[0] == false)
                            findDef(1); else document.getElementById("chat").innerHTML = "Judge: Player not your position";

                        setTimeout(function () { chat.innerHTML = "" }, 1000);
                    }
                    document.getElementById("seatPL12").onclick = function (evt) {
                        if (PL1mustDEF == true && statusColoda == "close")
                            document.getElementById("chat").innerHTML = "Judge: Player open deck";
                        if (PL1mustDEF == true && statusColoda == "open") if (seatsPl1[0] == false)
                            findDef(2);
                        else document.getElementById("chat").innerHTML = "Judge: Player not your position";
                        setTimeout(function () { chat.innerHTML = "" }, 1000);
                    }
                    document.getElementById("seatPL13").onclick = function (evt) {
                        if (PL1mustDEF == true && statusColoda == "close")
                            document.getElementById("chat").innerHTML = "Judge: Player open deck";
                        if (PL1mustDEF == true && statusColoda == "open") if (seatsPl1[0] == false)
                            findDef(3);
                        else document.getElementById("chat").innerHTML = "Judge: Player not your position";
                        setTimeout(function () { chat.innerHTML = "" }, 1000);
                    }
                    document.getElementById("seatPL14").onclick = function (evt) {
                        if (PL1mustDEF == true && statusColoda == "close")
                            document.getElementById("chat").innerHTML = "Judge: Player open deck";
                        if (PL1mustDEF == true && statusColoda == "open") if (seatsPl1[3] == false && seatsPl1[2] == false && seatsPl1[1] == false)
                            findDef(4);
                        else document.getElementById("chat").innerHTML = "Judge: Player not your position";
                        setTimeout(function () { chat.innerHTML = "" }, 1000);
                    }
                }
            }
        }
        //повідомлення*??
    }

    function toggleExpandOrCollapse() {
        if (MenuAfterGame.style.display === "none") {
            MenuAfterGame.style.display = "block";
            MenuAfterGame.style.opacity = "1";
        } else {
            MenuAfterGame.style.opacity = "0";
            MenuAfterGame.style.display = "none";
        }
    }

    function completeBack(e) {
        backDeck = e;
        imgCardPl1.src = backDeck;
        imgCardPl2.src = backDeck;
    }
    function err_completeBack() {
        backDeck = "../images/back_deck/back1.png";
        imgCardPl1.src = backDeck;
        imgCardPl2.src = backDeck;
    }
    function completeFront(e) {
        frontDeck = e;
    }
    function err_completeFront() {
        frontDeck = "../images/theme_cards1/";
    }

    // Вспомогательная функция установки обработчика события
    function addHandler(event, handler) {
        if (document.attachEvent) {
            document.attachEvent('on' + event, handler);
        }
        else if (document.addEventListener) {
            document.addEventListener(event, handler, false);
        }
    }

    // Вспомогательная функция принудительного снятия выделения
    function killSelection() {
        if (window.getSelection) {
            window.getSelection().removeAllRanges();
        }
        else if (document.selection && document.selection.clear) {
            document.selection.clear();
        }
    }

    function load() {
        local.readText("BackDeck.txt").done(function (fileContents) {
            if (fileContents == null) err_completeBack()
            else  completeBack(fileContents); });

        local.readText("FrontDeck.txt").done(function (fileContents) {
            if (fileContents == null) err_completeFront()
            else completeFront(fileContents); });

        local.readText("Background.txt").done(function (fileContents) {
            if (fileContents == null) { pageBody.style.backgroundColor = "#a14341"; MenuAfterGame.style.backgroundColor = "#a14341"; }
            else { pageBody.style.backgroundColor = fileContents; MenuAfterGame.style.backgroundColor = fileContents }
        });

        local.readText("Field.txt").done(function (fileContents) {
            if (fileContents == null) content.style.backgroundColor = "#a14341";
            else content.style.backgroundColor = fileContents; });
    }

    // Функция обработчика нажатия клавиш
    function noSelectionEvent(event) {
        var event = event || window.event;

        // При нажатии на Ctrl+A и Ctrl+U убрать выделение
        // и подавить всплытие события
        var key = event.keyCode || event.which;
        if (event.ctrlKey && (key == 65 || key == 85)) {
            killSelection();
            if (event.preventDefault) { event.preventDefault(); }
            else { event.returnValue = false; }
            return false;
        }
    }

    var page = WinJS.UI.Pages.define("/html/pageGameOnePL.html", {
        ready: function (element, options) {
            addHandler('keydown', noSelectionEvent);
            addHandler('keyup', noSelectionEvent);
            load();
            showMenu();
            document.getElementById("chat").innerHTML = "Judge: Move the player";
            document.getElementById("out_total_PL1").innerHTML = " goals:" + totalPL1;
            document.getElementById("out_total_PL2").innerHTML = " goals:" + totalPL2;
            var max = 35, min = 0, l = 18;
            var arr = new Array(), m = [], n = 0, nn = 0, arr2 = [];
            if (max - min < l - 1) return;
            for (var i = 0; i <= (max - min) ; i++)
                m[i] = i + min;
            for (var i = 0; i < l; i++) {
                n = Math.floor(Math.random() * (m.length));
                arr[i] = m.splice(n, 1);
            }
            for (var i = 0; i < l; i++) {
                nn = Math.floor(Math.random() * (m.length));
                arr2[i] = m.splice(nn, 1);
            }
            document.getElementById("count_card1").innerHTML = "Player cards: " + arr.length;
            document.getElementById("count_card2").innerHTML = "Computer cards: " + arr2.length;
            arrayPL1 = arr;
            arrayPL2 = arr2;
            firstGoPL1();
            for (var ch = 0; ch < 5; ch++) {
                document.getElementById("seat_back_pl1" + ch).style.visibility = "hidden";
                document.getElementById("seat_back_pl2" + ch).style.visibility = "hidden";
                document.getElementById("seat_open_pl2" + ch).style.visibility = "hidden";
                document.getElementById("seat_open_pl1" + ch).style.visibility = "hidden";
                document.getElementById("comp_seat_back_pl1" + ch).style.visibility = "hidden";
                document.getElementById("comp_seat_back_pl2" + ch).style.visibility = "hidden";
                document.getElementById("comp_seat_open_pl2" + ch).style.visibility = "hidden";
                document.getElementById("comp_seat_open_pl1" + ch).style.visibility = "hidden";
                document.getElementById("play_compPL" + ch).style.visibility = "hidden";

            }
           setTimeout(function () { chat.innerHTML = "" }, 3000);
        }
    });
})();