﻿
(function () {
    "use strict";

    // Вспомогательная функция установки обработчика события
    function addHandler(event, handler) {
        if (document.attachEvent) {
            document.attachEvent('on' + event, handler);
        }
        else if (document.addEventListener) {
            document.addEventListener(event, handler, false);
        }
    }

    // Вспомогательная функция принудительного снятия выделения
    function killSelection() {
        if (window.getSelection) {
            window.getSelection().removeAllRanges();
        }
        else if (document.selection && document.selection.clear) {
            document.selection.clear();
        }
    }

    // Функция обработчика нажатия клавиш
    function noSelectionEvent(event) {
        var event = event || window.event;

        // При нажатии на Ctrl+A и Ctrl+U убрать выделение
        // и подавить всплытие события
        var key = event.keyCode || event.which;
        if (event.ctrlKey && (key == 65 || key == 85)) {
            killSelection();
            if (event.preventDefault) { event.preventDefault(); }
            else { event.returnValue = false; }
            return false;
        }
    }
    function goToPageGameHome() {
        document.location.href = "/default.html";
    }
    var page = WinJS.UI.Pages.define("/html/rules.html", {
        ready: function (element, options) {
            back.addEventListener("click", goToPageGameHome, false);
            addHandler('keydown', noSelectionEvent);
            addHandler('keyup', noSelectionEvent);
        }
    });
})();