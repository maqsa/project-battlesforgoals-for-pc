﻿
(function () {
    "use strict";

    var app = WinJS.Application;
    var activation = Windows.ApplicationModel.Activation;

    function goToPageGameTwoPL() {
        var url = document.getElementById("TwoPlayer");
        document.location.href = "/html/pageGameTwoPL.html";
    }
    function goToPageGameOnePL() {
        var url = document.getElementById("OnePlayer");
        document.location.href = "/html/pageGameOnePL.html";
    }
    function goToPageGameRules() {
        var url = document.getElementById("Rules");
        document.location.href = "/html/rules.html";
    }
    function goToPageGameTheme() {
        var url = document.getElementById("Theme");
        document.location.href = "/html/theme.html";
    }
    app.onactivated = function (args) {
        if (args.detail.kind === activation.ActivationKind.launch) {
            if (args.detail.previousExecutionState !== activation.ApplicationExecutionState.terminated) {
                // TODO: Это приложение было вновь запущено. Инициализируйте
                // приложение здесь.
            } else {
                // TODO: Это приложение вновь активировано после приостановки.
                // Восстановите состояние приложения здесь.
            }
            args.setPromise(WinJS.UI.processAll());
        }
    };

    app.oncheckpoint = function (args) {
        // TODO: Это приложение будет приостановлено. Сохраните здесь все состояния,
        // которые необходимо сохранять во время приостановки. Можно использовать
        // объект WinJS.Application.sessionState, который автоматически
        // сохраняется и восстанавливается при приостановке. Если перед приостановкой приложения необходимо
        // выполнить асинхронную операцию, вызовите метод
        // args.setPromise().
    };
    // Вспомогательная функция установки обработчика события
    function addHandler(event, handler) {
        if (document.attachEvent) {
            document.attachEvent('on' + event, handler);
        }
        else if (document.addEventListener) {
            document.addEventListener(event, handler, false);
        }
    }

    // Вспомогательная функция принудительного снятия выделения
    function killSelection() {
        if (window.getSelection) {
            window.getSelection().removeAllRanges();
        }
        else if (document.selection && document.selection.clear) {
            document.selection.clear();
        }
    }

    // Функция обработчика нажатия клавиш
    function noSelectionEvent(event) {
        var event = event || window.event;

        // При нажатии на Ctrl+A и Ctrl+U убрать выделение
        // и подавить всплытие события
        var key = event.keyCode || event.which;
        if (event.ctrlKey && (key == 65 || key == 85)) {
            killSelection();
            if (event.preventDefault) { event.preventDefault(); }
            else { event.returnValue = false; }
            return false;
        }
    }

    // Установить обработчики клавиатуры
    function nocopy(event) {
        var event = event || window.event;
        if (event.preventDefault) { event.preventDefault(); }
        else { event.returnValue = false; }
        return false;
    }

    var page = WinJS.UI.Pages.define("default.html", {
        ready: function (element, options) {
            playMenuItem.addEventListener("click", toggleExpandOrCollapse, false);
            OnePlayer.style.display = "none";
            TwoPlayer.style.display = "none";
            TwoPlayer.addEventListener("click", goToPageGameTwoPL, false);
            OnePlayer.addEventListener("click", goToPageGameOnePL, false);
            Rules.addEventListener("click", goToPageGameRules, false);
            Theme.addEventListener("click", goToPageGameTheme, false);
            addHandler('keydown', noSelectionEvent);
            addHandler('keyup', noSelectionEvent);
            document.onmousedown = nocopy;
            document.onmouseup = nocopy;
            document.onmousemove = nocopy;
            document.ondragstart = nocopy;
            document.onselectstart = nocopy;
            document.ontextmenu = nocopy;
            document.oncopy = nocopy; 

        }
    });

    function toggleExpandOrCollapse() {
        if (OnePlayer.style.display === "none" && TwoPlayer.style.display === "none") {
            expand(OnePlayer, document.querySelectorAll(".affectedItem"));
            expand(TwoPlayer, document.querySelectorAll(".affectedItem"));

            playMenuItem.innerText = "Select players";
        } else {
            collapse(OnePlayer, document.querySelectorAll(".affectedItem"));
            collapse(TwoPlayer, document.querySelectorAll(".affectedItem"));

            playMenuItem.innerText = "Play game";
        }
    }

    function expand(element, affected) {
        // Create expand animation.
        var expandAnimation = WinJS.UI.Animation.createExpandAnimation(element, affected);

        // Insert expanding item into document flow. This will change the position of the affected items.
        element.style.display = "block";
        element.style.position = "inherit";
        element.style.opacity = "1";

        // Execute expand animation.
        expandAnimation.execute();
    }

    function collapse(element, affected) {
        // Create collapse animation.
        var collapseAnimation = WinJS.UI.Animation.createCollapseAnimation(element, affected);

        // Remove collapsing item from document flow so that affected items reflow to their new position.
        // Do not remove collapsing item from DOM or display at this point, otherwise the animation on the collapsing item will not display
        element.style.position = "absolute";
        element.style.opacity = "0";

        // Execute collapse animation.
        collapseAnimation.execute().done(
            // After animation is complete, remove from display.
            function () { element.style.display = "none"; }
        );
    }
    app.start();
})();