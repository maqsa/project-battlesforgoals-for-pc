﻿(function () {
    "use strict";
    var local = WinJS.Application.local;

    function goToPageGameHome() {
        document.location.href = "/default.html";
    }

    function showList(){
        list_front_deck.style.display = "none";
        list_back_deck.style.display = "none";
        list_background.style.display = "none";
        list_colorField.style.display = "none";

        _frontDeck.addEventListener("click", showListFrontDeck, false);
        _backDeck.addEventListener("click", showListBackDeck, false);
        _background.addEventListener("click", showListBackground, false);
        _colorField.addEventListener("click", showListColorField, false);

        list_front_deck_Img1.addEventListener("click", showTableCards1, false);
        list_front_deck_Img2.addEventListener("click", showTableCards2, false);

        list_back_deck_Img1.addEventListener("click", showBackTableCards1, false);
        list_back_deck_Img2.addEventListener("click", showBackTableCards2, false);
        list_back_deck_Img3.addEventListener("click", showBackTableCards3, false);
        list_back_deck_Img4.addEventListener("click", showBackTableCards4, false);
        list_back_deck_Img5.addEventListener("click", showBackTableCards5, false);

        list_background1.addEventListener("click", showBackground1Table, false);
        list_background2.addEventListener("click", showBackground2Table, false);
        list_background3.addEventListener("click", showBackground3Table, false);
        list_background4.addEventListener("click", showBackground4Table, false);
        list_background5.addEventListener("click", showBackground5Table, false);

        list_colorField1.addEventListener("click", showColorField1Table, false);
        list_colorField2.addEventListener("click", showColorField2Table, false);
        list_colorField3.addEventListener("click", showColorField3Table, false);
        list_colorField4.addEventListener("click", showColorField4Table, false);
        list_colorField5.addEventListener("click", showColorField5Table, false);

    }

    function showListFrontDeck() {
        if (list_front_deck.style.display === "none") {
            list_front_deck.style.display = "block";
        } else {
            list_front_deck.style.display = "none";
        }
    }
    function showTableCards1() {
        tableImg1.src = "../images/theme_cards1/4.gif";
        tableImg2.src = "../images/theme_cards1/23.gif";
        tableImg3.src = "../images/theme_cards1/15.gif";
        tableImg4.src = "../images/theme_cards1/34.gif";
        tableImg5.src = "../images/theme_cards1/8.gif";
        var img = "../images/theme_cards1/";
        local.writeText("FrontDeck.txt", img);
        _frontDeck_select_img.src = "../images/theme_cards1/34.gif";
    }
    function showTableCards2() {
        tableImg1.src = "../images/theme_cards2/4.gif";
        tableImg2.src = "../images/theme_cards2/23.gif";
        tableImg3.src = "../images/theme_cards2/15.gif";
        tableImg4.src = "../images/theme_cards2/34.gif";
        tableImg5.src = "../images/theme_cards2/8.gif";
        var img = "../images/theme_cards2/";
        local.writeText("FrontDeck.txt", img);
        _frontDeck_select_img.src = "../images/theme_cards2/34.gif";
    }
    /*BACK DECK*/
    function showListBackDeck() {
        if (list_back_deck.style.display === "none") {
            list_back_deck.style.display = "block";
        } else {
            list_back_deck.style.display = "none";
        }
    }
    function showBackTableCards1() {
        var img = "../images/back_deck/back1.png";
        tableBack.src = "../images/back_deck/back1.png";
        local.writeText("BackDeck.txt", img);
        _backDeck_select_img.src = "../images/back_deck/back1.png";
    }

    function showBackTableCards2() {
        var img = "../images/back_deck/back2.png";
        tableBack.src = "../images/back_deck/back2.png";
        local.writeText("BackDeck.txt", img);
        _backDeck_select_img.src = "../images/back_deck/back2.png";
    }
    function showBackTableCards3() {
        var img = "../images/back_deck/back3.png";
        tableBack.src = "../images/back_deck/back3.png";
        local.writeText("BackDeck.txt", img);
        _backDeck_select_img.src = "../images/back_deck/back3.png";
    }
    function showBackTableCards4() {
        var img = "../images/back_deck/back4.png";
        tableBack.src = "../images/back_deck/back4.png";
        local.writeText("BackDeck.txt", img);
        _backDeck_select_img.src = "../images/back_deck/back4.png";
    }
    function showBackTableCards5() {
        var img = "../images/back_deck/back5.png";
        tableBack.src = "../images/back_deck/back5.png";
        local.writeText("BackDeck.txt", img);
        _backDeck_select_img.src = "../images/back_deck/back5.png";
    }
    /*BACKGROUND*/
    function showListBackground() {
        if (list_background.style.display === "none") {
            list_background.style.display = "block";
        } else {
            list_background.style.display = "none";
        }
    }
    function showBackground1Table() {
        var color = "#03872c";
        background_table.style.backgroundColor = "#03872c";
        local.writeText("Background.txt", color);
        _background_select_back.style.backgroundColor = "#03872c";
    }

    function showBackground2Table() {
        var color = "#025097";
        background_table.style.backgroundColor = "#025097";
        local.writeText("Background.txt", color);
        _background_select_back.style.backgroundColor = "#025097";
    }

    function showBackground3Table() {
        var color = "#aa0000";
        background_table.style.backgroundColor = "#aa0000";
        local.writeText("Background.txt", color);
        _background_select_back.style.backgroundColor = "#aa0000";
    }

    function showBackground4Table() {
        var color = "#1f1f1f";
        background_table.style.backgroundColor = "#1f1f1f";
        local.writeText("Background.txt", color);
        _background_select_back.style.backgroundColor = "#1f1f1f";
    }

    function showBackground5Table() {
        var color = "#a14341";
        background_table.style.backgroundColor = "#a14341";
        local.writeText("Background.txt", color);
        _background_select_back.style.backgroundColor = "#a14341";
    }
    /*color field*/
    function showListColorField() {
        if (list_colorField.style.display === "none") {
            list_colorField.style.display = "block";
        } else {
            list_colorField.style.display = "none";
        }
    }
    function showColorField1Table() {
        var color = "#03872c";
        color_field_table.style.backgroundColor = "#03872c";
        local.writeText("Field.txt", color);
        _colorField_select_back.style.backgroundColor = "#03872c";
    }
    function showColorField2Table() {
        var color = "#025097";
        color_field_table.style.backgroundColor = "#025097";
        local.writeText("Field.txt", color);
        _colorField_select_back.style.backgroundColor = "#025097";
    }
    function showColorField3Table() {
        var color = "#aa0000";
        color_field_table.style.backgroundColor = "#aa0000";
        local.writeText("Field.txt", color);
        _colorField_select_back.style.backgroundColor = "#aa0000";
    }
    function showColorField4Table() {
        var color = "#1f1f1f";
        color_field_table.style.backgroundColor = "#1f1f1f";
        local.writeText("Field.txt", color);
        _colorField_select_back.style.backgroundColor = "#1f1f1f";
    }
    function showColorField5Table() {
        var color = "#a14341";
        color_field_table.style.backgroundColor = "#a14341";
        local.writeText("Field.txt", color);
        _colorField_select_back.style.backgroundColor = "#a14341";
    }

    // Вспомогательная функция установки обработчика события
    function addHandler(event, handler) {
        if (document.attachEvent) {
            document.attachEvent('on' + event, handler);
        }
        else if (document.addEventListener) {
            document.addEventListener(event, handler, false);
        }
    }

    // Вспомогательная функция принудительного снятия выделения
    function killSelection() {
        if (window.getSelection) {
            window.getSelection().removeAllRanges();
        }
        else if (document.selection && document.selection.clear) {
            document.selection.clear();
        }
    }

    // Функция обработчика нажатия клавиш
    function noSelectionEvent(event) {
        var event = event || window.event;

        // При нажатии на Ctrl+A и Ctrl+U убрать выделение
        // и подавить всплытие события
        var key = event.keyCode || event.which;
        if (event.ctrlKey && (key == 65 || key == 85)) {
            killSelection();
            if (event.preventDefault) { event.preventDefault(); }
            else { event.returnValue = false; }
            return false;
        }
    }
    var page = WinJS.UI.Pages.define("/html/theme.html", {
        ready: function (element, options) {
            back.addEventListener("click", goToPageGameHome, false);
            showList();
            addHandler('keydown', noSelectionEvent);
            addHandler('keyup', noSelectionEvent);

            local.readText("BackDeck.txt").done(function (fileContents) {
                if (fileContents == null) {
                    tableBack.src = "../images/back_deck/back1.png";
                    _backDeck_select_img.src = "../images/back_deck/back1.png";
                }
                else {
                    tableBack.src = fileContents;
                    _backDeck_select_img.src = fileContents;
                }
            });

            local.readText("FrontDeck.txt").done(function (fileContents) {
                if (fileContents == null) {
                    tableImg1.src = "../images/theme_cards1/" + "4.gif";
                    tableImg2.src = "../images/theme_cards1/" + "23.gif";
                    tableImg3.src = "../images/theme_cards1/" + "15.gif";
                    tableImg4.src = "../images/theme_cards1/" + "34.gif";
                    tableImg5.src = "../images/theme_cards1/" + "8.gif";
                    _frontDeck_select_img.src = "../images/theme_cards1/" + "34.gif";
                }
                else {
                    tableImg1.src = fileContents + "4.gif";
                    tableImg2.src = fileContents + "23.gif";
                    tableImg3.src = fileContents + "15.gif";
                    tableImg4.src = fileContents + "34.gif";
                    tableImg5.src = fileContents + "8.gif";
                    _frontDeck_select_img.src = fileContents + "34.gif";
                }
            });

            local.readText("Background.txt").done(function (fileContents) {
                if (fileContents == null) {
                     background_table.style.backgroundColor = "#a14341";
                    _background_select_back.style.backgroundColor = "#a14341";
                }
                else {
                     background_table.style.backgroundColor = fileContents;
                    _background_select_back.style.backgroundColor = fileContents;
                }
            });

            local.readText("Field.txt").done(function (fileContents) {
                if (fileContents == null) {
                    color_field_table.style.backgroundColor = "#a14341";
                    _colorField_select_back.style.backgroundColor = "#a14341";
                }
                else {
                    color_field_table.style.backgroundColor = fileContents;
                    _colorField_select_back.style.backgroundColor = fileContents;
                }
            });
        }
    });


})();